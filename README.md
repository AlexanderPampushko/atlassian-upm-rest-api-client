## Project overview
Библиотека позволяет получать данные о плагинах Jira, Confluence, 
состоянии их лицензий и обновлять лицензии плагинов.  

Все операции выполняются через недокументированный REST API UPM (Universal Plugin Manager)
## Build project
 Для запуска проекта необходимо 
 
 * для IDE установить например Lombok Plugin for IntelliJ IDEA
 * Необходимо установить локально такую зависимость (__[Исходный код указанной библиотеки](https://bitbucket.org/AlexanderPampushko/my-atlassian-client)__)
 
```
<groupId>pmpshk.atlassian.customer-portal-client</groupId>
<artifactId>my-atlassian-client</artifactId>
<version>0.1.2</version>
```
Вы можете собрать указанную зависимость скачав исходники и запустив в корне проекта команду
```
mvn clean package install
```
 


## To Do
  
## Code Example


### Выводим данные о плагинах
```
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pmpshk.atlassian.upm.models.PluginListResult;
import pmpshk.atlassian.upm.models.plugin_license.PluginLicense;
import pmpshk.atlassian.upm.models.plugins_list.plugin.ThePlugin;
import pmpshk.atlassian.upm.models.single_plugin.SinglePlugin;
import pmpshk.atlassian.upm.models.update_plugin.UpdatePluginLicense;
import pmpshk.atlassian.upm.UpmClient;

import java.io.IOException;
import java.lang.invoke.MethodHandles;

/**
 *
 */
public class UpmClientTest
{
	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	public static void main(String[] args) throws IOException
	{
		UpmClient upmClient = UpmClient.newBuilder().baseUrl("https://your-confluence.test.net").login("admin")
				.password("password").build();
		
		PluginListResult plugins = upmClient.getPlugins();
		ThePlugin[] pluginsList = plugins.getPlugins();
		for (ThePlugin plugin : pluginsList)
		{
			if (plugin.isUsesLicensing())
			{
				PluginLicense pluginLicense = upmClient.getPluginLicense(plugin.getKey());
				String rawLicense = pluginLicense.getRawLicense();
				System.out.print(pluginLicense.getExpiryDateString() + " ");
				System.out.print(pluginLicense.getPluginKey() + " ");
				System.out.println(plugin.getName());
			}
		}
		
		System.out.println(plugins);
}
```

### Обновляем лицензию плагина
```
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pmpshk.atlassian.upm.UpmClient;
import pmpshk.atlassian.upm.models.single_plugin.SinglePlugin;
import pmpshk.atlassian.upm.models.update_plugin.UpdatePluginLicense;

import java.io.IOException;
import java.lang.invoke.MethodHandles;

/**
 *
 */
public class UpmClientTest
{
	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	public static void main(String[] args) throws IOException
	{
		UpmClient upmClient = UpmClient.newBuilder().baseUrl("https://your-confluence.test.net").login("admin")
				.password("password").build();
		
		final String pluginKey = "de.communardo.confluence.plugins.userprofile";
		
		UpdatePluginLicense obj = new UpdatePluginLicense();
		final String rawLicense = "AAABMA0ODAoPeNqVkd9rwjAQx9/zVwT23GJru6kQ2Kh9GNgqq+5pL7d6dYE0Kfkh879fYpWN4YuQQHL3zX0/d3molKS1OtJJ7tciSxdZRotmS9NJMiO16z9Rr7udQW1YlJAlmlbzwXIlWQjSjVYdF2hopzQtlOyEQ9nix4KWRxAOgpIUGs+HJVhkoXCUJNEkJ15vobU19MhgmE6zpzzLng89cBG3qr/my8oHbgp+PZjVDskeQ7h3EvRe+eMVJx6EO3BpYueZhxE5RmnDhRscHwveojT47lsNBVPibb1Egi9Qfg9cn/7wp4H/LrvGgvaGrANh7iT1M+DHC+VaH0ByMzadpFPSlDXzO1olaT6fzeePZDU2cpv5ktyeBjzPvVhXVflWvL6syMbp9gsM/v+mH4EuvDswLAIUDsm2uGeK3wSSjl9DrcxJUORLtwkCFHXhbiWTawW/JhjfZpm8aej5ZoD9X02f7";
		obj.setRawLicense(rawLicense);
		SinglePlugin updatePluginLicense = upmClient.updatePlugin(pluginKey, obj);
		PluginLicense pluginLicense = upmClient.getPluginLicense(pluginKey);
		boolean success = pluginLicense.getRawLicense().equals(rawLicense);
		System.out.println(success);
	}
}

```

## Contributors

Alexander Pampushko
   
## License
  
Apache License 2.0
