package pmpshk.atlassian.upm;

import lombok.extern.slf4j.Slf4j;
import okhttp3.ResponseBody;
import pmpshk.atlassian.upm.exceptions.GetPluginException;
import pmpshk.atlassian.upm.exceptions.GetPluginListException;
import pmpshk.atlassian.upm.exceptions.UpdatePluginException;
import pmpshk.atlassian.upm.models.PluginListResult;
import pmpshk.atlassian.upm.models.plugin_license.PluginLicense;
import pmpshk.atlassian.upm.models.single_plugin.SinglePlugin;
import pmpshk.atlassian.upm.models.update_plugin.UpdatePluginLicense;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

import java.io.IOException;

/**
 * Класс клиента для Yandex Market API
 * <br />
 */
@Slf4j
public class UpmClient
{
	/**
	 * Логин
	 * <br />
	 */
	String login;
	
	/**
	 * Пароль
	 * <br />
	 */
	String password;
	
	/**
	 * базовый URL
	 * <br />
	 */
	String baseUrl;
	
	/**
	 * Ссылка на экземпляр интерфейса UpmApi
	 * <br />
	 */
	private UpmApi upmApi;
	
	/**
	 * Приватный конструктор,
	 * <br />
	 * т.к. мы будем создавать экземпляр клиента через Builder
	 * <br />
	 */
	private UpmClient()
	{
	
	}
	
	///////////////////////////////////////////////////
	public class Builder
	{
		private Builder()
		{
		
		}
		
		public Builder login(String login)
		{
			UpmClient.this.login = login;
			return this;
		}
		
		public Builder password(String password)
		{
			UpmClient.this.password = password;
			return this;
		}
		
		public Builder baseUrl(String baseUrl)
		{
			UpmClient.this.baseUrl = baseUrl;
			return this;
		}
		
		/**
		 * настраиваем REST-адаптер, который будет использоваться для работы с UPM plugin API
		 * <br />
		 * И создаем Retrofit-клиента, согласно описанию API в интерфейсе {@link UpmApi}
		 * <br />
		 *
		 * @return
		 */
		public UpmClient build()
		{
			Retrofit retrofit = new RetrofitManagerUpm().getRetrofitForMarketApi(UpmClient.this);
			UpmClient.this.upmApi = retrofit.create(UpmApi.class);
			return UpmClient.this;
		}
	}
	///////////////////////////////////////////////////
	
	/**
	 * создаём новый builder для создания {@link UpmClient}
	 *
	 * @return экземпляр Builder-ра
	 */
	public static Builder newBuilder()
	{
		return new UpmClient().new Builder();
	}
	
	//////////////////////////////////////////////////////////////
	//                      Методы клиента
	//////////////////////////////////////////////////////////////
	
	/**
	 * @return Возвращает объект содержащий список плагинов и доп. информацию - {@code PluginList}
	 * <br />
	 */
	public PluginListResult getPlugins() throws IOException
	{
		//String jiraCookie = getJiraCookie();
		PluginListResult result = null;
		try
		{
			Call<PluginListResult> pluginListCall = upmApi.getPlugins();
			Response<PluginListResult> response = pluginListCall.execute();
			result = response.body();
		}
		catch (IOException ex)
		{
			log.error("error", ex);
		}
		
		if (result != null)
		{
			return result;
		}
		else
		{
			throw new GetPluginListException();
		}
		
	}
	
	/**
	 * получаем информацию по одному плагину, имеющему указанный pluginKey
	 *
	 * @param pluginKey
	 *
	 * @return
	 *
	 * @throws IOException
	 */
	public SinglePlugin getPlugin(final String pluginKey) throws IOException
	{
		SinglePlugin result = null;
		try
		{
			Call<SinglePlugin> pluginListCall = upmApi.getPlugin(pluginKey);
			Response<SinglePlugin> response = pluginListCall.execute();
			result = response.body();
		}
		catch (IOException ex)
		{
			log.error("error", ex);
		}
		
		if (result != null)
		{
			return result;
		}
		else
		{
			throw new GetPluginException();
		}
	}
	
	/**
	 * получаем информации о лицензии плагина имеющего указанный pluginKey
	 *
	 * @param pluginKey
	 * 		ключ плагина
	 *
	 * @return объекта содержащий данные лицензии
	 *
	 * @throws IOException
	 */
	public PluginLicense getPluginLicense(final String pluginKey) throws IOException
	{
		PluginLicense result = null;
		try
		{
			Call<PluginLicense> pluginListCall = upmApi.getPluginLicense(pluginKey);
			Response<PluginLicense> response = pluginListCall.execute();
			result = response.body();
		}
		catch (IOException ex)
		{
			log.error("error", ex);
		}
		
		if (result != null)
		{
			return result;
		}
		else
		{
			throw new GetPluginException();
		}
	}
	
	/**
	 * обновим лицензию для плагина имеющего ключ pluginKey
	 *
	 * @param pluginKey
	 * @param plugin
	 *
	 * @return
	 *
	 * @throws IOException
	 */
	public SinglePlugin updatePlugin(final String pluginKey, final UpdatePluginLicense plugin) throws IOException
	{
		SinglePlugin result = null;
		try
		{
			Call<SinglePlugin> pluginListCall = upmApi.updatePluginLicense(pluginKey, plugin);
			Response<SinglePlugin> response = pluginListCall.execute();
			if (!response.isSuccessful())
			{
				log.error("Update plugin is not successful.");
				ResponseBody errorBody = response.errorBody();
				if (errorBody != null)
				{
					byte[] bytes = errorBody.bytes();
					String error = new String(bytes, "UTF-8");
					log.error(error);
				}
			}
			result = response.body();
		}
		catch (IOException ex)
		{
			log.error("error", ex);
		}
		
		if (result != null)
		{
			return result;
		}
		else
		{
			throw new UpdatePluginException();
		}
	}
}
