package pmpshk.atlassian.upm.models.plugin_license;

import com.google.gson.annotations.SerializedName;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import pmpshk.atlassian.upm.models.BaseModel;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
public class PluginLicense extends BaseModel
{
	@SerializedName("maintenanceExpiryDate")
	private long maintenanceExpiryDate;
	
	@SerializedName("enterprise")
	private boolean enterprise;
	
	@SerializedName("pluginKey")
	private String pluginKey;
	
	@SerializedName("nearlyExpired")
	private boolean nearlyExpired;
	
	@SerializedName("subscription")
	private boolean subscription;
	
	@SerializedName("rawLicense")
	private String rawLicense;
	
	@SerializedName("valid")
	private boolean valid;
	
	@SerializedName("evaluation")
	private boolean evaluation;
	
	@SerializedName("expiryDate")
	private long expiryDate;
	
	@SerializedName("licenseType")
	private String licenseType;
	
	@SerializedName("creationDateString")
	private String creationDateString;
	
	@SerializedName("maintenanceExpiryDateString")
	private String maintenanceExpiryDateString;
	
	@SerializedName("upgradable")
	private boolean upgradable;
	
	@SerializedName("links")
	private PluginLicenseLinks links;
	
	@SerializedName("expiryDateString")
	private String expiryDateString;
	
	@SerializedName("typeI18nSingular")
	private String typeI18nSingular;
	
	@SerializedName("typeI18nPlural")
	private String typeI18nPlural;
	
	@SerializedName("organizationName")
	private String organizationName;
	
	@SerializedName("contactEmail")
	private String contactEmail;
	
	@SerializedName("dataCenter")
	private boolean dataCenter;
	
	@SerializedName("licenseTypeDescriptionKey")
	private String licenseTypeDescriptionKey;
	
	@SerializedName("active")
	private boolean active;
	
	@SerializedName("supportEntitlementNumber")
	private String supportEntitlementNumber;
	
	@SerializedName("renewable")
	private boolean renewable;
	
	@SerializedName("autoRenewal")
	private boolean autoRenewal;
}
