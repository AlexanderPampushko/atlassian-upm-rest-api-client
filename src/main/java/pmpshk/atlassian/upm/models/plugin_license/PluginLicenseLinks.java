package pmpshk.atlassian.upm.models.plugin_license;

import com.google.gson.annotations.SerializedName;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import pmpshk.atlassian.upm.models.BaseModel;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
public class PluginLicenseLinks extends BaseModel
{
	@SerializedName("license")
	private String license;
	
	@SerializedName("new")
	private String jsonMemberNew;
	
	@SerializedName("update-license")
	private String updateLicense;
	
	@SerializedName("self")
	private String self;
	
	@SerializedName("alternate")
	private String alternate;
	
	@SerializedName("license-callback")
	private String licenseCallback;
}
