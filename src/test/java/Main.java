import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pmpshk.atlassian.customer_portal_client.MyAtlassianClient;
import pmpshk.atlassian.upm.UpmClient;
import pmpshk.atlassian.upm.models.plugins_list.plugin.ThePlugin;
import pmpshk.atlassian.upm.models.single_plugin.SinglePlugin;
import pmpshk.atlassian.upm.models.update_plugin.UpdatePluginLicense;

import java.io.IOException;
import java.io.InputStream;
import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/**
 *
 */
public class Main
{
	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	private static MyAtlassianClient client;
	
	static
	{
		String login = Settings.getGoogleLogin();
		String password = Settings.getGooglePassword();
		
		try
		{
			client = new MyAtlassianClient();
			if (client.loginToGoogleServices(login, password))
			{
				if (client.loginToMyAtlassianWithGoogleCredentials())
				{
					log.info("успешно вошли в My Atlassian через аккаунт Google");
				}
				else
				{
					log.error("Не удалось войти в My Atlassian через аккаунт Google");
				}
			}
			else
			{
				log.error("Не удалось залогиниться в Google");
			}
		}
		catch (Exception ex)
		{
			log.error("ошибка", ex);
		}
	}
	
	public static void main(String[] args) throws IOException
	{
		Properties settings = new Properties();
		//todo сделать нормально
		InputStream resource = UpmClient.class.getClassLoader().getResourceAsStream("test.properties");
		settings.load(resource);
		
		UpmClient upmClientConfluence = UpmClient.newBuilder().baseUrl(settings.getProperty("confluence.baseUrl"))
				.login(settings.getProperty("confluence.login"))
				.password(settings.getProperty("confluence.password"))
				.build();
		
		List<ThePlugin> plugins = Arrays.asList(upmClientConfluence.getPlugins().getPlugins());
		ArrayList<String> pluginKeysList = new ArrayList();
		for (ThePlugin plugin : plugins)
		{
			if (plugin.isUserInstalled() && plugin.isUsesLicensing())
			{
				String pluginKey = plugin.getKey();
				System.out.println(pluginKey);
				pluginKeysList.add(pluginKey);
				
			}
		}
		List<String> licensesList = generateLicenses(pluginKeysList);
		
		for (int i = 0; i < pluginKeysList.size(); i++)
		{
			UpdatePluginLicense updatePluginLicense = new UpdatePluginLicense();
			updatePluginLicense.setRawLicense(licensesList.get(i));
			
			int count = 0;
			int maxTries = 3;
			boolean success = false;
			while (!success && count++ < maxTries)
			{
				try
				{
					System.out.println("начали обновление " + pluginKeysList.get(i));
					SinglePlugin plugin = upmClientConfluence.updatePlugin(pluginKeysList.get(i), updatePluginLicense);
					System.out.println(plugin);
					success = true;
					Thread.sleep(1000);
				}
				catch (Exception ex)
				{
					log.error("еще одна ошибка при установке лицензии для плагина {} . Попытка {} ",
							pluginKeysList.get(i), count);
				}
			}
			if (!success)
			{
				log.error("прошли максимальное количество попыток");
				log.error("Установить лицензию для плагина {} не удалось, количесто попыток {}",
						pluginKeysList.get(i), maxTries);
			}
		}
	}
	
	public static List<String> generateLicenses(List<String> pluginKeysList) throws IOException
	{
		List<String> resultLiscensList = new ArrayList<>();
		for (String key : pluginKeysList)
		{
			resultLiscensList.add(client.generateNewEvaluationLicense(key));
		}
		return resultLiscensList;
	}
}
