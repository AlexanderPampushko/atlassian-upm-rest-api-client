import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pmpshk.atlassian.upm.UpmClient;

import java.io.IOException;
import java.io.InputStream;
import java.lang.invoke.MethodHandles;
import java.util.Properties;

/**
 *
 */
public class Settings
{
	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	private static UpmClient upmClientConfluence;
	private static UpmClient upmClientJira;
	private static String googleLogin;
	private static String googlePassword;
	
	static
	{
		Properties settings = new Properties();
		//todo сделать нормально
		InputStream resource = UpmClient.class.getClassLoader().getResourceAsStream("test.properties");
		try
		{
			settings.load(resource);
		}
		catch (IOException e)
		{
			log.error("не удалось загрузить properties-файл");
		}
		
		upmClientConfluence = UpmClient.newBuilder().baseUrl(settings.getProperty("confluence.baseUrl"))
				.login(settings.getProperty("confluence.login"))
				.password(settings.getProperty("confluence.password"))
				.build();
		upmClientJira = UpmClient.newBuilder().baseUrl(settings.getProperty("jira.baseUrl"))
				.login(settings.getProperty("jira.login"))
				.password(settings.getProperty("jira.password"))
				.build();
		
		googleLogin = settings.getProperty("google.login");
		googlePassword = settings.getProperty("google.password");
	}
	
	public static UpmClient getUpmClientConfluence()
	{
		return upmClientConfluence;
	}
	
	public static UpmClient getUpmClientJira()
	{
		return upmClientJira;
	}
	
	public static String getGoogleLogin()
	{
		return googleLogin;
	}
	
	public static String getGooglePassword()
	{
		return googlePassword;
	}
}
