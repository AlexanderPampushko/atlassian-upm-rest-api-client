import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pmpshk.atlassian.upm.UpmClient;
import pmpshk.atlassian.upm.models.PluginListResult;
import pmpshk.atlassian.upm.models.plugin_license.PluginLicense;
import pmpshk.atlassian.upm.models.plugins_list.plugin.ThePlugin;
import pmpshk.atlassian.upm.models.single_plugin.SinglePlugin;
import pmpshk.atlassian.upm.models.update_plugin.UpdatePluginLicense;

import java.io.IOException;
import java.lang.invoke.MethodHandles;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 *
 */
public class UpmClientTest
{
	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	@Test
	public void getPluginInfoJira() throws IOException
	{
		getPluginInfo(Settings.getUpmClientJira());
	}
	
	@Test
	public void getPluginInfoConfluence() throws IOException
	{
		getPluginInfo(Settings.getUpmClientConfluence());
	}
	
	public void getPluginInfo(UpmClient upmClient) throws IOException
	{
		PluginListResult plugins = Settings.getUpmClientJira().getPlugins();
		ThePlugin[] pluginsList = plugins.getPlugins();
		assertTrue(pluginsList.length > 0);
		for (ThePlugin plugin : pluginsList)
		{
			System.out.println(plugin.getKey());
			assertTrue(!plugin.getKey().isEmpty());
		}
	}
	
	
	@Test
	@Disabled
	//todo для каждого случая запуска теста необходимо получать свежий лицензионный ключ
	public void setPluginLicenseConfluence() throws IOException
	{
		final String pluginKey = "eu.mirrorlake.plugins.jira2excel";
		final String rawLicense = "AAABKg0ODAoPeNqFkctqwzAQRff6CkHXNpbyagOGFseLQOyUJu2qm6mYpGplSYzkkPx97SR9UAJZCIRmdO6R5qZyltdux7MRF9lUyqmY8GK15jITt6xumzek5eY5IIU8EWyGQZH2UTubV5rIkYFP5OVeoeHzxjuKSK9TXu7AtNC3sYLwuJlBxLynJkIk2ZgVzkZQsYYGc/CDwXAyGg7vtw1okyrXfNfLqju42PCbkUdqkWGbNj9KqTftVtuQfmgCib1f2tH0Dk/NRiu0AV+6d/UAyboYG9GCVVjuvabDH1/Z+y5pC1aHU6CQA7Yq67xbyULIcSayuzu2OEEv37+qh10+edLhrHimrQ8ej59ULKuqfCrmD4vrrFWEfhL5BkxA9tiSeoeA/0fwBU1wrCowLAIUdMtX+rrg2XuCrwG9nT8L5230u1UCFCitaqqbcjRLHJcv+hUBWz87c3pMX02eu";
		
		SinglePlugin updatePluginLicense = setPluginLicense(Settings.getUpmClientConfluence(), pluginKey, rawLicense);
		PluginLicense pluginLicense = Settings.getUpmClientConfluence().getPluginLicense(pluginKey);
		assertEquals(pluginLicense.getRawLicense(), rawLicense);
	}
	
	@Test
	@Disabled
	public void setPluginLicenseJira() throws IOException
	{
		final String pluginKey = "eu.mirrorlake.plugins.jira2excel";
		
		UpdatePluginLicense obj = new UpdatePluginLicense();
		final String rawLicense = "AAABKg0ODAoPeNqFkctqwzAQRff6CkHXNpbyagOGFseLQOyUJu2qm6mYpGplSYzkkPx97SR9UAJZCIRmdO6R5qZyltdux7MRF9lUyqmY8GK15jITt6xumzek5eY5IIU8EWyGQZH2UTubV5rIkYFP5OVeoeHzxjuKSK9TXu7AtNC3sYLwuJlBxLynJkIk2ZgVzkZQsYYGc/CDwXAyGg7vtw1okyrXfNfLqju42PCbkUdqkWGbNj9KqTftVtuQfmgCib1f2tH0Dk/NRiu0AV+6d/UAyboYG9GCVVjuvabDH1/Z+y5pC1aHU6CQA7Yq67xbyULIcSayuzu2OEEv37+qh10+edLhrHimrQ8ej59ULKuqfCrmD4vrrFWEfhL5BkxA9tiSeoeA/0fwBU1wrCowLAIUdMtX+rrg2XuCrwG9nT8L5230u1UCFCitaqqbcjRLHJcv+hUBWz87c3pMX02eu";
		obj.setRawLicense(rawLicense);
		SinglePlugin updatePluginLicense = Settings.getUpmClientJira().updatePlugin(pluginKey, obj);
		PluginLicense pluginLicense = Settings.getUpmClientJira().getPluginLicense(pluginKey);
		assertEquals(pluginLicense.getRawLicense(), rawLicense);
	}
	
	public SinglePlugin setPluginLicense(UpmClient upmClient, String pluginKey, String rawLicense) throws IOException
	{
		UpdatePluginLicense obj = new UpdatePluginLicense();
		obj.setRawLicense(rawLicense);
		SinglePlugin plugin = upmClient.updatePlugin(pluginKey, obj);
		return plugin;
	}
}
